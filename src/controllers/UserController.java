package controllers;
 
import java.util.Date;
import java.util.List;
 
import javax.servlet.http.HttpServletRequest;
 
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
 
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
 
@Controller
@RequestMapping("/user")
public class UserController {
 
	@RequestMapping(value="/addUserPage", method = RequestMethod.GET)
	public String getAddUserPage(ModelMap model) {
 
		return "addUser";
 
	}
 
	@RequestMapping(value="/add", method = RequestMethod.POST)
	public ModelAndView add(HttpServletRequest request, ModelMap model) {
 
		String name = request.getParameter("name");
		String email = request.getParameter("email");
 
	        Key userKey = KeyFactory.createKey("User", name);
 
		Date date = new Date();
                Entity user = new Entity("User", userKey);
                user.setProperty("name", name);
                user.setProperty("email", email);
                user.setProperty("date", date);
 
                DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
                datastore.put(user);
 
                return new ModelAndView("redirect:list");
 
	}
 
	@RequestMapping(value="/update/{name}", method = RequestMethod.GET)
	public String getUpdateUserPage(@PathVariable String name, 
			HttpServletRequest request, ModelMap model) {
 
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Query query = new Query("User");
		query.addFilter("name", FilterOperator.EQUAL, name);
		PreparedQuery pq = datastore.prepare(query);
 
		Entity e = pq.asSingleEntity();
		model.addAttribute("user",  e);
 
		return "updateUser";
 
	}
 
	@RequestMapping(value="/update", method = RequestMethod.POST)
	public ModelAndView update(HttpServletRequest request, ModelMap model) {
 
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
 
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String originalName =  request.getParameter("originalName");
 
		Query query = new Query("User");
		query.addFilter("name", FilterOperator.EQUAL, originalName);
		PreparedQuery pq = datastore.prepare(query);
		Entity user = pq.asSingleEntity();
 
		user.setProperty("name", name);
		user.setProperty("email", email);
		user.setProperty("date", new Date());
 
                datastore.put(user);
 
               //return to list
               return new ModelAndView("redirect:list");
 
	}
 
	@RequestMapping(value="/delete/{name}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable String name,
			HttpServletRequest request, ModelMap model) {
 
                DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
 
                Query query = new Query("User");
		query.addFilter("name", FilterOperator.EQUAL, name);
		PreparedQuery pq = datastore.prepare(query);
		Entity user = pq.asSingleEntity();
 
                datastore.delete(user.getKey());
 
                //return to list
                return new ModelAndView("redirect:../list");
 
	}
 
	//get all users
	@RequestMapping(value="/list", method = RequestMethod.GET)
	public String listUser(ModelMap model) {
 
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Query query = 
                      new Query("User").addSort("date", Query.SortDirection.DESCENDING);
	        List<Entity> users = 
                      datastore.prepare(query).asList(FetchOptions.Builder.withLimit(10));
 
	        model.addAttribute("userList",  users);
 
		return "listUser";
 
	}
 
}