package controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

@Controller
@RequestMapping("/login")
public class LoginController {

	@RequestMapping(value = "userlogin", method = RequestMethod.GET)
	public String LoginPage(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();

		resp.setContentType("text/html");
		// resp.getWriter().println("<h2>GAE - Integrating Google user account</h2>");

		if (user == null)  {

			return "redirect:"
					+ userService.createLoginURL(req.getRequestURI());

		}
		return "/indexstaff";

		// return null;
	}
	
	
	
	
	
	
	@RequestMapping(value = "userlogout", method = RequestMethod.GET)
	public String LogoutPage(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();

		resp.setContentType("text/html");
		// resp.getWriter().println("<h2>GAE - Integrating Google user account</h2>");

		if (user != null) {


			return "redirect:"
			+ userService.createLoginURL(req.getRequestURI());
			
		} 
		return "index";

		// return null;
	}
	
	
}
