
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--[if lt IE 7 ]> <html class="no-js ie6" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr"> <![endif]-->
<!--[if IE 7 ]>   <html class="no-js ie7" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr"> <![endif]-->
<!--[if IE 9 ]>    <html class="no-js ie9" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"
	dir="ltr">
<!--<![endif]-->

<head>
<title>BFC Motors | Premium Racing Vehicles</title>
<!--[if IE ]><meta http-equiv="X-UA-Compatible" content="IE=Edge" /><![endif]-->
<link rel="shortcut icon" href="/sites/all/themes/tesla/favicon.ico"
	type="image/x-icon" />
<link rel="apple-touch-icon" href="apple-touch-icon.png" />
<link type="text/css" rel="stylesheet" media="all" href="/css/temp.css" />
<link type="text/css" rel="stylesheet" media="all" href="/css/css_main.css" />

<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>

<script src="http://code.jquery.com/jquery.quicksand.js"></script>
<style type="text/css">
/* add additional selectors to hide elements while web fonts are loading */
.wf-loading li a,.wf-loading h1.page-title-header {
	visibility: hidden;
}
</style>

<script type="text/javascript" src="../js/js_main.js">
</script>
<script type="text/javascript" src="../js/unslider.js">
</script>
</head>

<body id="page-homepage" class="front  page-home i18n-en">

	<!--[if lte IE 6]><script src="/sites/all/themes/tesla/js/ie6/warning.js"></script><script>window.onload=function(){e("/sites/all/themes/tesla/js/ie6/")}</script><![endif]-->
	<!-- !!!!!!!!!! Do not insert any content before this tag -->
	<div class="wrapper">
		<div id="content-wrapper">
			<!--**************** ############## **********  NAVIGATION *****************-->
			<div id="main-nav">

				<div id="main-nav-back" class="short">
					<div id="main-nav-wrapper">

						<div id="branding" class="">
							<div id="logo" class="">
								<a href="index.jsp"><img src="../images/tesla_flag.png" alt="Tesla logo" width="78"
									height="105" /></a>
							</div>
							<!--End logo-->
						</div>
						<!-- End branding -->



						<ul class="links main-menu with-hover">
							<li class="menu header-only menu-models first"><a
								class="a_menu" href="/models.jsp" class="header-only menu-models"
								title="">Add Cars</a></li>
							<li class="menu-gallery header-only"><a class="a_menu"
								href="/customer/listCustomer" class="menu-gallery header-only">Customers</a></li>
						<li class="menu-services"><a class="a_menu" href="/user/list"
								href="/customer/listCustomer"class="menu-services" title="">Manage Users</a></li>
							<li class="menu-enthusiasts"><a class="a_menu" href="/enthusiasts.jsp"
								class="menu-enthusiasts" title="">Profile</a></li>

<!-- 							<li class="menu-contact_us"><a class="a_menu" -->
<!-- 								href="/contactus.jsp" class="menu-contact_us" title="">Contact -->
<!-- 									Us</a></li> -->



						</ul>

					</div>
					<!-- End main-nav-wrapper-->

				</div>
				<!-- End main nav back -->

				<div id="slide-nav-main">
					<div id="slide-nav-wrapper">

						<!-- 						Edit here and add nissan like drop down -->
						<div class='subnav first header-only menu-models'>
							<ul>
								<li><a href="/models/design" title="">Order</a></li>
								<li><a href="/models/drive" title="">Test Drive</a></li>
								<li><a href="/models/features" class="menu-features"
									title="">Features</a></li>
							</ul>
							<ul>
								<li><a href="/models/specs" class="menu-specs" title="">Specs</a></li>
								<li><a href="/goelectric" title="">Top 5 Questions</a></li>
								<li><a href="/charging" title="">Charging</a></li>
							</ul>
							<ul>
								<li><a href="/models/photo-gallery" class="menu-gallery"
									title="">Gallery</a></li>
								<li><a href="/models/walkthrough" title="">Videos</a></li>
							</ul>
						</div>




						<div class='subnav menu-gallery header-only'>
							<ul>
								<li><a href="/own#/model-x" title="">Videos</a></li>
								<li><a href="/own#/model-x" title="">Photos</a></li>
							</ul>
						</div>


						<div class='subnav active-trail menu-enthusiasts'>
							<ul>
								<li><a href="/blog" class="menu-blog active" title="">Blog</a></li>
								<li><a href="/news" title="">News</a></li>
							</ul>
						</div>

						<div class='subnav menu-services'>
							<ul>
								<li><a href="/accessories" title="">Accessories</a></li>
								<li><a href="/service" title="">Service Centers</a></li>
							</ul>
						</div>

						<div class='subnav menu-contact_us'>
							<ul>
								<li><a href="/contact_us" title="" class="active">Contact
										Us</a></li>
								<li><a href="/chat">Chat</a></li>
							</ul>

						</div>


					</div>
				</div>
				<!-- End slide-nav-->

				<div id="ribbon" class="short">
					<div id="secon-nav-wrapper"></div>
				</div>
				<!-- End ribbon -->



			</div>