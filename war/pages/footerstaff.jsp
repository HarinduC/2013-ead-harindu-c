

		



		<!-- End content wrapper-->
	</div>
	<!-- End wrapper -->
	<!-- ###################### FOOTER #################### -->
	<div id="locale_pop" class="blip">
		<p>
			<a href="#" class="close">X</a> <span id="locale_welcome"></span><span
				id="locale_msg"></span> <a href="/" id="locale_link"
				class="locale-link plain first" rel="">Deutschland Site Besuchen</a>
		</p>
	</div>
	<footer>
	<div id="footer-region">

		<div id="footer-wrapper" class="region">
			<div id="footermap">
				<ul class="car-links">

					<ul id="social">
						<li><a class="twitter" href="http://twitter.com/teslamotors"
							target="_blank">Twitter</a></li>
						<li><a class="facebook"
							href="http://www.facebook.com/teslamotors" target="_blank">Facebook</a></li>
						<li><a class="google"
							href="http://plus.google.com/+teslamotors" target="_blank">Google+</a></li>
						<li><a class="vimeo" href="http://vimeo.com/teslamotors"
							target="_blank">Vimeo</a></li>
					</ul>
				</ul>
			</div>
			<!-- End footermap -->

			<div id="secondary-centered">
				<ul class='links'>
					<li class=" first"><span id="copyright" title="">Copyright
							� 2013 Tesla Motors, Inc. All rights reserved.</span></li>
					<li class=""><a href="/about/legal" title="">Legal &amp;
							Privacy</a></li>

					<li class=""><a href="/firstresponders" title="">First
							Responders</a></li>

					<li class="locale-selector"><a href="http://#"
						class="locale-selector" id="choose-locale">Choose Region</a></li>

					<li class=" last"><div id='left-border'></div>
						<ul id='mytesla-button-menu'>
							<li><a href='/mytesla'><span>MY TESLA</span></a>
								&nbsp;&nbsp;|</li>
							<li>&nbsp;&nbsp;<a href="/contact">Contact</a> &nbsp;&nbsp;|
							</li>
							<li>&nbsp;&nbsp;<a href="/login/userlogout" id="mytesla-button">Sign
									In</a></li>
						</ul>
						<div id='right-border'></div></li>
				</ul>
			</div>
		</div>

		<div id="select-locale-overlay"></div>
	</footer>




</body>
</html>
