<%@include file="header.jsp" %>



			<div id="page">

				<!-- Content ($content)-->
				<div data-role="content" data-inset="true">

					<!-- Hero Area -->
					<div id="hero" class="">

						<div class="panel-pane pane-custom pane-1">



							<div class="pane-content">
								<div id="hero-background">
									<div id="hero-scaler">
										<div class="banner">
											<ul>
												<li
													style="background-image: url('/images/hero_20131119.jpg');">
													<h1>The jQuery slider that just slides.</h1>
													<p>No fancy effects or unnecessary markup, and it’s
														less than 3kb.</p> <a class="btn" href="#download">Download</a>
												</li>

												<li
													style="background-image: url('../images/hero_20131119.jpg');">

												</li>
												<li
													style="background-image: url('../images/hero_20131119.jpg');">

												</li>


											</ul>
											<!-- 										<img src="images/hero_normal_1800x595.jpg" alt="" /> -->

											<!-- 										<img src="images/hero_20131119.jpg" alt="" /> -->

										</div>



									</div>
									<!-- End hero-scaler -->
								</div>
								<!-- End hero-background -->
							</div>


						</div>
						<div class="panel-region-separator"></div>
						<div class="panel-pane pane-custom pane-2">



							<div class="pane-content">
								<div id="hero-container">



									<div id="main-models-copy">
										<h1>Model S</h1>
										<p class="sub-header">Zero emissions. Zero compromises.</p>
									</div>
									<!-- End main-models-copy -->

									<div id="main-cta">

										<div class="main-cta-right">
											<div class="div-model-s">
												<p class="model-s">Model S</p>
											</div>
										</div>
										<div id="models-landing-buttons">
											<a href="/models/design" class="button red first">Order</a> <a
												href="/models/drive" class="button grey">Test Drive</a>
										</div>
										<!-- End models-landing-buttons -->
										<div id="contact-links">
											<a href="#callback" data-order="1" class="triangle-arrow">Request
												a call back</a><br /> <a href="#callback" data-order="2"
												class="triangle-arrow">Apply for financing</a><br /> <a
												href="#callback" data-order="3" class="triangle-arrow">Value
												your trade-in</a><br />
										</div>
										<!--contact-links -->
									</div>
									<!-- End main-cta -->


								</div>
								<!-- End hero-container -->
							</div>


						</div>

					</div>
					<!-- / Hero Area -->


				</div>


				<!-- / Content -->
			</div>
			<!-- / page -->
			
				<div class="push"></div>
		</div>

<%@include file="footer.jsp" %>

