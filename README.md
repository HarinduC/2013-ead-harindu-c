
Case Study 2
BFC Company
BFC is in the business of building a number of different street racing cars. They have a portal to sell the cars as well as a web service that allows cars to be purchased in bulk. 
Customers who wish to purchase a car may contact a car salesman via Google chat which is built into the system. Once decided on the specification that they desire, an order can be placed via the public interface. 
BFC should be able to add inventory in the form of cars of different types and these are categorized by engine type. When an employee of BFG adds a car to the system, an appropriate engine for the car needs to be ordered via the integration services. Cars can be added as a single unit, in which case the availability of the engine should verified already or as a batch job in which case, the duration to fulfill the order should be a return parameter with the total cost of the order placed.
BFC may have stocks at hand as well.
